var gps = require("gps-tracking");
var sinoAdapter = require ('./sinotrack');
var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    port: '8889',
    database : 'gps_system'
});

/*con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});*/

var options = {
    'debug'                 : false, //We don't want to debug info automatically. We are going to log everything manually so you can check what happens everywhere
    'port'                  : 8191,
    'device_adapter'        : sinoAdapter
}

var server = gps.server(options,function(device,connection){

    device.on("connected",function(data){

        console.log("I'm a new device connected");
        return data;

    });

    

    device.on("login_request",function(device_id,msg_parts){

        console.log('Hey! I want to start transmiting my position. Please accept me. My name is '+device_id);

        this.login_authorized(true); 

        console.log("Ok, "+device_id+", you're accepted!");

    });
    

    device.on("ping",function(data){
        //this = device
        // console.log("I'm here: "+data.latitude+", "+data.longitude+" ("+this.getUID()+")");
        console.log(this.getUID().device_id);
        /*const sql = "INSERT INTO `ping_gps` (`gpsid`, `time`, `latitude`, `longitude`, `speed`, `orientation`) VALUES ("
               + "'" + this.getUID() + "', "
               + "'" + new Date(data.time).toISOString().split('T')[0] + ' ' + new Date(data.time).toISOString().split('T')[1].split('.')[0] + "', "
               + "'" + data.latitude + "', "
               + "'" + data.longitude + "', "
               + "'" + data.speed + "', "
               + "'" + data.orientation + "')";*/

        /*con.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Result: " + result);
        });*/

        //Look what informations the device sends to you (maybe velocity, gas level, etc)
        // console.log(data);  
        return data;

    });

   device.on("alarm",function(alarm_code,alarm_data,msg_data){
        console.log("Help! Something happend: "+alarm_code+" ("+alarm_data.msg+")");
    }); 

    //Also, you can listen on the native connection object
    connection.on('data',function(data){
        //echo raw data package
        //console.log(data.toString()); 
    })

});