// some functions you could use like this 
// f = require('gps-tracking/functions') // . There are optionals
// f = require("../functions");

exports.protocol="H02";
exports.model_name="ST901";
exports.compatible_hardware=["ST901/supplier"];

var adapter = function(device) {
	if( !(this instanceof adapter) ) return new adapter(device);
	
    this.format = { 
        "start": "*",
        "end": "#",
        "separator": ","
    };
	this.device = device;
	this.parse_data = function(data){
		data = data.toString();
		var parts={
			"device_id" 	: data,//mandatory
			"cmd": 'undefin'
		};
		parts.action = 'ping';
		return parts;
		const data_striped = data.split(',');
		let GPSID;
		let CMD = 'undefined';
		let TIME;
		let EFFECTIVE_MARK;
		let LATITUDE;
		let LATITUDE_DIRECTION;
		let LONGITUDE;
		let LONGITUDE_DIRECTION;
		let SPEED;
		let DIRECTION;
		let DATE;
		
		if (data_striped[2] === 'V1') { // Normal Information
			GPSID = data_striped[1];
			TIME = data_striped[3];
			EFFECTIVE_MARK = data_striped[4];
			LATITUDE = data_striped[5];
			LATITUDE_DIRECTION = data_striped[6];
			LONGITUDE = data_striped[7];
			LONGITUDE_DIRECTION = data_striped[8];
			SPEED = data_striped[9];
			DIRECTION = data_striped[10];
			DATE = data_striped[11];
		} else { // Confirmation Information
			GPSID = data_striped[1];
			CMD = data_striped[3];
			TIME = data_striped[4];
			EFFECTIVE_MARK = data_striped[5];
			LATITUDE = data_striped[6];
			LATITUDE_DIRECTION = data_striped[7];
			LONGITUDE = data_striped[8];
			LONGITUDE_DIRECTION = data_striped[9];
			SPEED = data_striped[10];
			DIRECTION = data_striped[11];
			DATE = data_striped[12];
		}
		var parts={
			"device_id" 	: GPSID,//mandatory
			"cmd" 			: CMD, //mandatory

			"date"			: DATE,
			"latitude"		: this.calcLat(LATITUDE, LATITUDE_DIRECTION == 'N' ? 1 : -1),
			"longitude"	    : this.calcLon(LONGITUDE, LONGITUDE_DIRECTION == 'E' ? 1 : -1),
			"speed"			: SPEED,
			"time"			: TIME,
			"orientation"	: DIRECTION
		};

		if (CMD === 'undefined') {
			parts.action = 'ping';
		} else {
			parts.action = 'other';
		}
		return parts;
	}

	this.calcLat = function (lat, dir) {
		const dg = Number(lat.substr(0,2));
		const min = Number(lat.substr(2, lat.length))/60;
		const res = ((dg + min) * dir).toFixed(6);
		return res;
	};

	this.calcLon = function (lon, dir) {
		const dg = Number(lon.substr(0,3));
		const min = Number(lon.substr(3, lon.length))/60;
		const res = ((dg + min) * dir).toFixed(6);
		return res;
	};

	this.authorize = function () {
	};
	
	this.request_login_to_device = function(){
		//@TODO: Implement this.
		this.device.login_authorized(true);
	}    
	
	this.receive_alarm = function(msg_parts){
		//@TODO: implement this
		
		//gps_data = msg_parts.data.substr(1);
		alarm_code = msg_parts.data.substr(0,1);
		alarm = false;
		switch(alarm_code.toString()){
			case "0":
				alarm = {"code":"power_off","msg":"Vehicle Power Off"};
				break;
			case "1":
				alarm = {"code":"accident","msg":"The vehicle suffers an acciden"};
				break;
			case "2":
				alarm = {"code":"sos","msg":"Driver sends a S.O.S."};
				break;
			case "3":
				alarm = {"code":"alarming","msg":"The alarm of the vehicle is activated"};
				break;
			case "4":
				alarm = {"code":"low_speed","msg":"Vehicle is below the min speed setted"};
				break;
			case "5":
				alarm = {"code":"overspeed","msg":"Vehicle is over the max speed setted"};
				break;
			case "6":
				alarm = {"code":"gep_fence","msg":"Out of geo fence"};
				break;
		}
		this.send_comand("AS01",alarm_code.toString());
		return alarm
	}
	
	
	this.get_ping_data = function(msg_parts){

		/*var data = {
			"date"			: msg_parts.date,
			"latitude"		: msg_parts.latitude,
			"longitude"	    : msg_parts.longitude,
			"speed"			: msg_parts.speed,
			"time"			: msg_parts.time,
			"orientation"	: msg_parts.orientation,
		};
		var d = data.date.slice(0,2);
		var m = data.date.slice(2,4);
		var a = data.date.slice(4,6);
		a = '20' + a;
		var date_aux = new Date(a + '/' + m + '/' + d).setUTCHours(23,26,22);
		res = {
			latitude		: data.latitude,
			longitude		: data.longitude,
			time			: date_aux,
			speed			: data.speed,
			orientation	    : data.orientation,
		}*/
		return msg_parts;	
	}
	
	this.send_comand = function(cmd,data){
		var msg = [this.device.uid,cmd,data];
		this.device.send(this.format_data(msg));
	}
	this.format_data = function(params){
		/* FORMAT THE DATA TO BE SENT */
		var str = this.format.start;
		if(typeof(params) == "string"){
			str+=params
		}else if(params instanceof Array){
			str += params.join(this.format.separator);
		}else{
			throw "The parameters to send to the device has to be a string or an array";
		}
		str+= this.format.end;
		return str;	
	}
}
exports.adapter = adapter;
